# Manual de integrações com clientes e parceiros

## Ambiente do editor

Abaixo você um passo a passo de como subir o ambiente de edição na sua máquina.

### Download do mkdocs

https://www.mkdocs.org/#installation

### Clone do projeto via GIT

Pergunte pro coleguinha engenheiro ao lado. =)

### Rodando o serviço na sua máquina

Acesse a pasta do projeto clonado e rode o seguinte comando:

```sh
mkdocs serve
```

Entre no seu navegador de preferência e acesse http://localhost:8000.

Tudo que você editar nesse projeto é atualizado automaticamente no navegador web.

## Subindo suas alterações

Basicamente, você precisa adicionar todas as suas mudanças num commit git e subir isso para o GitLab.

Depois que você terminar as mudanças, esses são os passos para colocar em produção:

```sh
# atualiza o projeto
git pull

# sobe as alterações
git add .
git commit -m "UMA BREVE EXPLICAÇÃO DO QUE FOI ALTERADO"
git push origin master
```
