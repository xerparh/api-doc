# Dados Organizacionais de Empresa
### Entidade: Company
Aqui você confere todas os dados organizacionais de empresa que você pode consutar e gravar na Xerpa. 

## Lista de Escritórios
### Company > Office

|  Campo  | Descrição | Forma de Preenchimento  | Variável |
| --- | --- | --- | --- |
|  Nome do Local  | Nome do Escritório  | Texto  | name |
|  CEP | CEP do Escritório | Texto  | zipcode |
|  Tipo | Tipo do Logradouro | Texto  | addrtype |
|  Logradouro | Logradouro do Escritório | Texto  | address1 |
|  Número  | Número do Escritório | Número | number |
|  Complemento  | Complemento do Escritório | Texto  | address1 |
|  Estado  | Estado do Escritório | Texto  | state |
|  Cidade | Cidade do Escritório | Número  | cityId |
|  Bairro | Bairro do Escritório | Texto  | district |

## Lista de Departamentos
### Company > Department

|  Campo  | Descrição | Forma de Preenchimento  | Variável |
| --- | --- | --- | --- |
|  Departamento  | Nome do Departamento  | Texto | name |

## Lista de Cargos/Níveis
### Company > Positions/Levels


|  Campo  | Descrição | Forma de Preenchimento  | Variável |
| --- | --- | --- | --- |
|  Cargo  | Nome do Cargo  | Texto  | name |
|  Níveis | Níveis do Cargo  | Texto  | name |

## Lista de Centros de Custo 
### Company > Cost Center

|  Campo  | Descrição | Forma de Preenchimento  | Variável |
| --- | --- | --- | --- |
|  Centro de Custo  | Nome do Centro de Custo  | Texto | name |


## Lista de Unidades de Negócio
### Company > Office

|  Campo  | Descrição | Forma de Preenchimento  | Variável |
| --- | --- | --- | --- |
|  Unidades de Negócio | Nome da Unidade de Negócios | Texto | name |

## Lista de Sindicatos
### Company > Unions

|  Campo  | Descrição | Forma de Preenchimento  | Variável |
| --- | --- | --- | --- |
|  Código Sindical  | Código do Sindicato  | Texto | abbreviation |
|  Nome do Sindicato  | Nome do Sindicato  | Texto | name |

## Lista de CNPJS
### Company > CNPJS

|  Campo  | Descrição | Forma de Preenchimento  |  Variável |
| --- | --- | --- | --- |
|  CNPJ | Número do CNPJ  | Número | cnpj
|  Nome Fantasia  | Nome Fantasia do CNPJ | Texto | name |
|  Razão Social  | Razão Social do CNPJ  | Texto | legalName |