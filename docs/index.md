# Manual de integrações Xerpa

Bem vindo ao manual de integração com os sistemas Xerpa.
Nesta página você encontrará as informações necessárias para utilizar nossa API. Os itens abordados nesta pequena introdução incluem os métodos para autenticação da API, o ambiente para realizar os testes das requisições e exemplos de consultas na API utilizando [GraphQL][graphql_site].

[GraphQL][graphql_site] trata-se de uma linguagem de consulta para _APIs_ criada pelo [Facebook][facebook_graphql], que permite obter toda a informação em uma única solicitação, ao contrário de uma consulta de API usando _REST_, que comumente requer múltiplas interações com o servidor.

## Ambiente de Testes

A Xerpa fornece um ambiente de testes isolado com características semelhantes ao ambiente de produção. Nós chamamos este ambiente de [sandbox][sandbox].

Você pode criar uma conta em Sandbox [aqui][sandbox_create].

Um _token_[^token] criado no ambiente de [sandbox][sandbox_create] não é compartilhado com o ambiente de [produção][producao] e vice-versa. Portanto, sempre que desejar utilizar o ambiente [sandbox][sandbox] você precisa registrar uma empresa e registrar um _token_[^token].

!!! warning "Importante"
    Todos os dados criados no ambiente [sandbox][sandbox] são apagados semanalmente.

## Api Playground

[API Playground](https://sandbox.xerpa.com.br/api/playground) é uma
instância do [graphiql](https://github.com/graphql/graphiql) que
funciona como uma ferramenta interativa no navegador para explorar uma
_API GraphQL_. Com essa ferramenta é possível testar suas solicitações
de maneira interativa e navegar pela estrutura da API.

Todos os exemplos nessa documentação podem ser usados no
_playground_. Para isso copie a soliticação juntamente com as
variáveis para os locais apropriados no _graphiql_.

![Playground openned in sandbox][playground_img]

[facebook_graphql]: http://facebook.github.io/graphql/
[playground_img]: /img/playground.png
[sandbox_create]: https://sandbox.xerpa.com.br/company-signup/
[sandbox]: https://sandbox.xerpa.com.br/
[producao]: https://app.xerpa.com.br
[graphql_site]: http://graphql.org/

[^token]: Token é um código de acesso para uma determinada aplicação. [Veja mais](/usando_api/)
