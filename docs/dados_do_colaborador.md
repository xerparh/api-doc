# Dados do Colaborador
###Entidade: Profile

Aqui você confere todos os dados de colaborador que você pode consultar e gravar na Xerpa. 

## Dados Pessoais - Identificação
### Profile

|  Campo | Descrição  | Forma de Preenchimento  | Variável |
| --- | --- | --- | --- |
|  Nome Completo  | Nome completo do colaborador | Texto  | name |
|  Nome Social  | Nome social do colaborador | Texto  | preferredName |
|  Data de Nascimento  | Data de nascimento do colaborador | Data | birthday |
|  Estado Civil | Estado civil do colaborador | Concubinato <br/> Divorciado(a) <br/> Separado(a) judicialmente <br/> Casado(a) <br/> Solteiro <br/> Não especificado(a) <br/> União estável <br/> Viúvo(a) | marital_status
|  Gênero  | Gênero do colaborador | Feminino<br/>Masculino<br/>Não especificado | gender
|  Nacionalidade | Nacionalidade do colaborador | Lista* (Global) | nationality | 
|  Naturalidade - Cidade | Estado de naturalidade do colaborador | Lista* (Global) | state |
|  Naturalidade - Cidade | Cidade de naturalidade do colaborador | Lista* (Global) | naturality_city |
|  Nome da Mãe | Nome da mãe do colaborador | Texto | mothers_name  |
|  Nome do Pai | Nome do pai do colaborador | Texto | fathers_name  |
|  Cor ou Raça | Cor ou raça do colaborador | Amarela<br/>Branca<br/>Indigena<br/>Negra<br/>Parda<br/>Não Informado | skin_color  
|  Deficiência | Deficiência do colaborador | Auditiva<br/>Intelectual<br/>Mental<br/>Múltipla<br/>Nenhuma<br/>Física<br/>Fala<br/>Visual | disabilities

## Dados Pessoais - Contatos Pessoais 
### Profile > personalContact

|  Campo | Descrição  | Forma de Preenchimento  | Variável |
| --- | --- | --- | --- |
|  Email  | Email do colaborador | Texto | email
|  Telefone Fixo  | Telefone fixo do colaborador | Numérico  |  phone
|  Celular | Telefone celular do colaborador | Numérico  | mobile

## Dados Pessoais - Contatos de Emergência (0 a n)
###Profile > emergencyContact

|  Campo | Descrição  | Forma de Preenchimento  | Variável |
| --- | --- | --- | --- |
|  Nome  | Nome do contato de emergência | Texto  | name |
|  Grau de Parentesco  | Grau de parentesco do contato com o colaborador | Texto | relationship |
|  Telefone Fixo  | Telefone do contato de emergência  | Numérico  | phone |

## Dados Pessoais - Cartão de Saúde (SUS)
### Profile > sus

|  Campo | Descrição  | Forma de Preenchimento  | Variável |
| --- | --- | --- | --- |
|  Número  | Número do cartão do SUS | Texto | number |

## Dados Pessoais - RG|CPF
### Profile > rg

|  Campo | Descrição  | Forma de Preenchimento  | Variável |
| --- | --- | --- | --- |
|  Nº RG  | Número de RG do colaborador | Numérico  | number |
|  Data de Expedição | Data de expedição do RG do colaborador | Data | issue_date |
|  Órgão Emissor | Órgão emissor do RG do colaborador | Texto | issuer |
|  UF  | Estado de emissão do RG do colaborador | Texto | state_id |

### Profile > cpf
|  CPF | CPF do colaborador | Numérico  | number |

## Dados Pessoais - Dados Bancários 
### Entidade > bankAccounts

|  Campo | Descrição  | Forma de Preenchimento  | Variável |
| --- | --- | --- | --- |
|  Banco  | Banco onde o colaborador possui conta | Numérico  | agency |
|  Agência | Agência do banco onde o colaborador possui conta | Numérico  |
|  Conta | Número da conta do colaborador com o dígito | Numérico  | account |
|  Tipo | Tipo da conta | Conta Corrente<br/>Conta Salário<br/>Conta Poupança<br/>Conta Conjunta | account_type |

## Dados Pessoais - Carteira de Reservista
### Profile > cam (certificacao de alistamento militar)

|  Campo | Descrição  | Forma de Preenchimento  | Variável |
| --- | --- | --- | --- |
|  Número  | Número da carteira de reservista do colaborador | Numérico  | number

## Dados Pessoais - CNH
### Profile > cnh

|  Campo | Descrição  | Forma de Preenchimento  | Variável |
| --- | --- | --- | --- |
|  CNH | Número de Registro  | Número da CNH do colaborador | number |
|  CNH  | Categoria  | Categoria da CNH do colaborador | category |
|  CNH | Data de Emissão | Data de emissão da CNH do colaborador | issueDate |
|  CNH  | Validade | Data de emissão da CNH do colaborador | expirationDate |

## Dados Pessoais - Endereço Residencial
### Profile > personalAddress

|  Campo | Descrição  | Forma de Preenchimento  | Variável |
| --- | --- | --- | --- |
|  CEP  | Número do CEP do endereço do colaborador | Texto | zipcode |
|  Tipo  | Tipo de Logradouro do endereço do colaborador | Texto | type |
|  Logradouro  | Lodragouro do endereço do colaborador | Texto | address1 |
|  Número  | Número do endereço do colaborador | Numérico  | number |
|  Complemento  | Complemento do endereço do colaborador | Texto | address2 |
|  Bairro  | Bairro de residência do colaborador | Texto | district |
|  Estado | Estado de residência do colaborador | Texto | state |
|  Cidade | Cidade  de residência do colaborador | Texto | city |

## Dados Pessoais - Dependentes (0 a n)
### Profile > Dependents

|  Campo | Descrição  | Forma de Preenchimento  | Variável |
| --- | --- | --- | --- | 
|  Nome  | Nome do dependente do colaborador | Texto | name |
|  Tipo  | Tipo de dependência do dependente | Texto | dependentType |

## Dados Pessoais - Formação Escolar
### Profile > education

|  Campo | Descrição  | Forma de Preenchimento  | Variável 
| --- | --- | --- | --- |
|  Curso  | Curso frequentado pelo colaborador | Texto | field |
|  Instituição  | Instituição de ensino frequentada pelo colaborador | Texto | school |
|  País onde cursou | País onde o colaborador cursou a faculdade | Texto | country |
|  Estado onde cursou | Estado onde o colaborador cursou a faculdade | Texto | state |
|  Tipo de curso  | Tipo de curso frequentado pelo colaborador  | Bacharelado<br/>Doutorado<br/>Ensino Fundamental I<br/>Especialização<br/>Ensino Médio<br/>Licenciatura<br/>Mestrado<br/>MBA<br/>Ensino Fundamental II<br/>Pós-doutorado<br/>Tecnólogo<br/>Técnico de nível médio | degree |
|  Duração  | Duração do curso | Longa (acima de 360 horas)<br/>Média (acima de 40 horas, até 360 horas)<br/>Curta (até 40 horas) | duration |
|  Situação Atual | Situação atual do curso | Curso Concluído<br/>Interrompido<br/>Cursando | situation |
|  Data de Conclusão | Data de conclusão do curso | Data | conclusion |

## Dados Pessoais - PIS/NIT
### Profile > pis

|  Campo | Descrição  | Forma de Preenchimento  | Variável |
| --- | --- | --- | --- |
|  Número do PIS | Número do PIS do colaborador | Numérico  | number |

## Dados Pessoais - Carteira de Trabalho
### Profile > ctps

|  Campo | Descrição  | Forma de Preenchimento  | Variável |
| --- | --- | --- | --- |
|  Número  | Número da carteira de trabalho do colaborador | Numérico  | number |
|  Série | Número da série da carteira de trabalho | Numérico  | series |
|  UF  | Estado de emissão da carteira de trabalho | Texto | state |
|  Data de Emissão | Data da emissão da carteira de trabalho | Data | createdAt |

## Dados Pessoais - Contribuição Sindical

|  Campo | Descrição  | Forma de Preenchimento  |
| --- | --- | --- | 
|  Mês/Ano da contribuição sindical | Mês/Ano da ultima contribuição sindical | MM/AAAA |

## Dados Pessoais - Dados Contratuais
###Profile

|  Campo | Descrição  | Forma de Preenchimento  | Variável |
| --- | --- | --- | --- |
|  Regime | Regime de contratação do colaborador | Intermitente<br/>Aprendiz<br/>Autônomo(a)<br/>CLT<br/>Empregado(a) Doméstico(a)<br/>Estagiário(a)<br/>PJ<br/>Sócio(a) - Divisão de Lucros<br/>Sócio(a) - Pro labore<br/>Estatutário(a)<br/>Temporário(a)<br/>Voluntário(a) | employmentContract
|  Tipo de Salário  | Periodicidade de pagamento  | Por Dia<br/>Por Hora<br/>Por Mês<br/>Por Tarefa<br/>Por Quinzena<br/>Por Semana | salaryPeriod |
|  Salário (R$) | Salário bruto do colaborador | Numérico  | salary |
|  Departamento  | Departamento do colaborador | Lista* (por empresa) | department |
|  Centro de Custo  | Centro de custo do colaborador | Lista* (por empresa) | costCenter |
|  CBO | CBO da profissão do colaborador | Lista* (por empresa) | cbo |
|  Cargo  | Cargo do colaborador | Lista* (por empresa) | position |
|  Nível  | Nível do cargo do colaborador | Lista* (por empresa) | positionLevel |
|  Unidade de Negócio | Unidade de negócio do colaborador | Lista* (por empresa) | businessUnit |
|  Há pagamento de hora extra? | O colaborador recebe por horas extras? | Verdadeiro/Falso | overtimeAllowed |
|  Bate o ponto? | O colaborador bate o ponto no dia-a-dia? | Verdadeiro/Falso | punchcardRequired |
|  Cargo de confiança? | O colaborador possui cargo de confiança? | Verdadeiro/Falso | trustedPosition |
|  Escritório de Trabalho | Escritório de trabalho do colaborador | Lista* (por empresa) | office |
|  Sindicato | Sindicato do colaborador | Sindicatos  | union |
|  Jornada de Trabalho | Jornada de trabalho do colaborador | Lista* (por empresa) | workload |
|  Tipo  | Tipo de jornada de trabalho do colaborador | Fixa<br/>Flexível | type |
|  Horas Semanais | Somatório das horas semanais da jornada | Calculadas a partir da jornada | load |
|  Escala | Escala do colaborador | Calculadas a partir da jornada | regimen |
|  Data de Admissão | Data de admissão do colaborador | Data | admissionDate |
|  Periodos de Experiência | Períodos de experiência do colaborador | 1x60 dias<br/>1x90 dias<br/>2x30 dias<br/>2x45 dias<br/>30 + 60 dias<br/>Personalizado | probationPeriods |
|  Fim do 1º Período de Experiência | Fim do 1º período de experiência | Data | probationDate1 |
|  Fim do 2º Período de Experiência | Fim do 2º período de experiência | Data | probationDate2 |

## Dados Contratuais - Contato Profissional 
### Profile > businessContact

|  Campo | Descrição  | Forma de Preenchimento  | Variáveis |
| --- | --- | --- | --- |
|  Email  | Email profissional do colaborador | Texto | email |
|  Telefone Fixo  | Telefone fixo profissional do colaborador | Numérico | phone |
|  Ramal  | Ramal profissional do colaborador | Numérico | branch |
|  Celular | Celular profissional do colaborador | Numérico | mobile |
 
## Dados Contratuais - Gestores (0 a n)
### Profile > managers

|  Campo | Descrição  | Forma de Preenchimento  | Variáveis |
| --- | --- | --- | --- |
|  Gestores | Gestores do colaborador | Lista* (por empresa) | managers |

## Campos Customizados (Categoria e Formulário)
### Profile > customFields

|  **Campo** | **Descrição** | **Forma de Preenchimento** | **Variável** |
| :--- | :--- | :--- | :--- |
|  Formulário (slug) | slug-id do Formulário | Texto | nameSlug |
|  Título do Formulário | Título do Formulário | Texto | name |
|  Id do Formulário | Id do Formulário | Texto | classId |
|  Descrição | Descrição do Formulário | Texto | description |
|  Confirmação | É necessária confirmação? | Booleano | confirmation |
|  Categoria (slug) | slug-id da categoria | Texto | configurationNameSlug |
|  Título da Categoria | Título da Categoria | Texto | configurationName |
|  Id da Categoria | Id da Categoria | Texto | configurationId |

## Campos Customizados (Campo)
### Profile > customFields > attributeValues

|  **Campo** | **Descrição do campo** | **Forma de Preenchimento** | **description** |
| --- | --- | :--- | --- |
|  Tipo | Tipo do campo | Texto<br/>Lista <br/>Radio<br/>Data<br/>Anexo<br/>Numérico | type |
|  Editável? | O campo é editável? | Boolean | editable |
|  Obrigatório? | O campo é obrigatório? | Boolean | required |
|  Título | Titulo do campo | Texto | name |
|  Descrição | Descrição do campo | Texto | description |
|  Id | Id do campo | GUID | id |

## Campos Customizados (Valores Selecionados/Preenchidos)
### Profile > customFields > selectedValues

|  **Campo** | **Descrição do campo** | **Forma de Preenchimento** | **Description** |
| --- | --- | :--- | --- |
|  Valor | Valor preenchido ou selecionado do campo | Texto<br/>Lista <br/>Radio<br/>Data<br/>Anexo<br/>Numérico | value |
|  ID do campo | Id da opção selecionada ou do valor preenchido | Numérico | id |



**as listas podem ser consultadas através nossa API de [dados organizacionais](/dados_organizacionais)*

**os valores listados para preenchimento são valores de negócio, ou seja, valores de interface com o usuário. Para saber quais valores utilizar via API consulte as variáveis via sandbox.*
