# Consulta

Na Xerpa, você pode consultar dados organizacionais para que a experiência da integração fique transparente e sem entraves para o usuário final.

Abaixo, você confere uma query com todos os dados organizacionais que pode consultar na Xerpa. Fique à vontade 
para utilizar os que mais fizerem sentido para o seu caso.

Exemplo:
```graphql 
query {
  company(id: "SEU_COMPANY_ID_AQUI") {
    offices {
      id
      name
      # ESCOLHA O QUE VOCÊ PRECISA!
      # dentro do playground da Xerpa, voce pode usar o autocomplete (ctrl + espaco)
      # e ver todos os campos disponiveis dentro de company.
    }
    businessUnit { 
      id 
    }
    cnpjs {
      id
    }
  }
}
```


## Entidades Disponíveis para Consulta 

| Nome no graphql | Entidade |
| ---             | --- |
| offices         | [Escritórios](../dados_organizacionais.md#lista-de-escritorios) |
| departments     | [Departamentos](../dados_organizacionais.md#lista-de-departamentos) |
| positions       | [Cargos](../dados_organizacionais.md#lista-de-cargosniveis) |
| costCenters     | [Centros de Custo](../dados_organizacionais.md#lista-de-centros-de-custo) |
| businessUnits   | [Unidades de Negócio](../dados_organizacionais.md#lista-de-unidades-de-negocio) |
| unions          | [Sindicatos](../dados_organizacionais.md#lista-de-sindicatos) | 
| cnpjs           | [CNPJs](../dados_organizacionais.md#lista-de-cnpjs) |


# Inserindo Dados Organizacionais

Você também pode enviar dados organizacionais para a Xerpa, o que deve ser o caso se você estiver integrando um 
sistema de Folha de Pagamento```graphql s ou um ERP. 

Nesse caso, você pode utilizar as mutations abaixo para inserir os dados de company.

## CNPJ's
```graphql 
mutation {
	cnpj_upsert(input: { 
    company_id: "SEU_COMPANY_ID_AQUI",
    cnpj: "62.082.824/0001-05", 
    name: "teste", 
    legal_name: "teste"
  }) {
		id
	}
}
```

## Escritorio
```graphql 
mutation {
	office(
		input: {
            company_id: "SEU_COMPANY_ID_AQUI"
			name: "local teste"
			address: {
				city_id: "5173",
				number: "123",
				zipcode: "00000-000",
				address1: "teste",
				address2: "casa 1",
				addrtype: ALAMEDA,
				district: "teste",
			}
		}
	) {
		id
	}
}
```

## Unidade de Negocio

```graphql 
mutation {
	businessUnit(input: { 
    companyId: "SEU_COMPANY_ID_AQUI", 
    name: "unit_teste"
    }) {
      id
      name
	}
}
```

## Departamento
```graphql 
mutation {
	department(input: { 
    companyId: "SEU_COMPANY_ID_AQUI", 
    name: "unit_teste"
     }) {
      id
      name
	}
}
```

## Centro de Custo
```graphql 
mutation {
	costCenter(input: { companyId: "SEU_COMPANY_ID_AQUI", name: "cost_teste" }) {
		id
	}
}
```

## Cargos + Níveis
```graphql 
mutation {
	alter_positions(
		company_id: "SEU_COMPANY_ID_AQUI",
		positions: [
        { 
          name: "cargo", 
          levels: [
            { name: "jr" }, 
            { name: "pl" }, 
            { name: "sr" }
          ] 
        }]
	) {
		positions {
			id
		}
	}
}

```


# Integracao com ID Externo

Este tipo de integração só existe quando o sistema integrado com a API da Xerpa precisar inserir/atualizar/sincronizar informações de **Company**.
Para isso, é necessário informar o Hash e ID do sistema de origem.


Todas as mutations listadas acima podem receber tambem o ID do sistema que esta integrando via API, adicionando ao input
o ID Externo e o hash do parceiro, conforme exemplos abaixo:

```graphql
partner: {
  partnerHash: "SEU_PARTNER_HASH_AQUI",
  externalId: "123ABC", 
} 
```

**Exemplo com departamentos:**
```graphql
mutation {
	department(input: { 
    companyId: "SEU_COMPANY_ID_AQUI", 
    name: "unit_teste",
    partner: {
      partnerHash: "SEU_PARTNER_HASH_AQUI",
      externalId: "123ABC", 
    } 
     }) {
      id
      name
	}
}
```

Para solicitar o **Partner Hash** é necessário entrar em contato através do e-mail [suporteintegracoes@xerpa.com.br](mailto:suporteintegracoes@xerpa.com.br).

Você pode conferir todos os dados organizacionais que temos disponíveis para consulta [aqui](/dados_organizacionais).
