# O que é um Webhook?

Um webhook é um sistema de notificação de eventos. Quando um evento em específico ocorrer na Xerpa, é disparado um JSON com o GUID da entidade na Xerpa e o evento ao qual ela foi submetida para uma URL cadastrada no webhook.

A partir do recebimento do webhook disparado, o sistema destino pode tomar as providências que desejar através da nossa API. 

Um exemplo de uso é o de finalização da admissão, onde um colaborador que acabou de ser admitido na Xerpa dispara um evento de admission_done e o GUID do colaborador para a URL cadastrada pelo desenvolvedor. 

Assim, o desenvolvedor pode utilizar nossa API para consultar os dados desse novo colaborador e atualizar a base de colaboradores do sistema destino.

Na Xerpa, você pode monitorar os seguintes eventos das seguintes entidades:

##Tabela de Eventos e Entidades

|  **Evento** | **Entidade** | **Descrição** | **Disparo** | **Enum** |
| :---: | :---: | :---: | :---: | :---: |
|  Admissão Iniciada | Colaborador | Início da admissão de um colaborador na Xerpa. | É disparado a partir do envio de um convite de cadastro ao colaborador. | admission_started |
|  Admissão Manual Iniciada | Colaborador | Início da admissão manual de um colaborador na Xerpa. | É disparador a partir do cadastro de um novo colaborador na Xerpa. | admission_manually_invited |
|  Admissão Finalizada | Colaborador | Fim da admissão do colaborador. | É disparado quando o DP/RH muda o status do colaborador de "Em Admissão" para "Ativo". | admission_done |
|  Demissão Iniciada | Colaborador | Início da demissão na Xerpa. | É disparado quando o DP/RH muda o status do colaborador de "Ativo" para "Em Desligamento". | termination_done |
|  Demissão Finalizada | Colaborador | Fim da demissão na Xerpa. | É disparado quando o DP/RH muda o status do colaborador de "Em Desligamento" para "Desligado". | termination_started |
|  Alteração e Inclusão de Departamento  | Empresa | Alteração ou Inclusão de um departamento específico na empresa.  | É disparado quando é incluído ou alterado um departamento na Xerpa.  | department_upserted |
|  Deleção de Departamento | Empresa | Deleção de um departamento na empresa.  | É disparado quando é deletado um departamento na Xerpa.  | department_deleted |
|  Alteração e Inclusão de Unidade de Negócios | Empresa | Alteração ou inclusão de uma unidade de negócios específico na empresa.  | É disparado quando é incluído ou alterado uma unidade de negócios na Xerpa.  | business_unit_upserted |
|  Deleção de Unidade de Negócios | Empresa | Deleção de uma unidade de negócios na empresa. | É disparado quando é deletada uma unidade de negócios na Xerpa.  | business_unit_deleted |
|  Alteração e Inclusão de Escritório  | Empresa | Alteração ou Inclusão de um escritório específico na empresa.  | É disparado quando é incluído ou alterado um escritório na Xerpa.  | office_upserted |
|  Deleção de Escritório | Empresa | Deleção de novo escritório na empresa.  | É disparado quando é deletado um escritório na Xerpa.  | office_deleted |
|  Alteração e Inclusão de Sindicato | Empresa | Alteração ou Inclusão de um sindicato específico na empresa.  | É disparado quando é incluído ou alterado um sindicato na Xerpa.  | union_upserted |
|  Deleção de Sindicato | Empresa | Deleção de um sindicato na empresa.  | É disparado quando é deletado um sindicato na Xerpa.  | union_deleted |
|  Alteração e Inclusão de Centro de Custo  | Empresa | Alteração ou Inclusão de um centro de custo específico na empresa.  | É disparado quando é incluído ou alterado um centro de custo na Xerpa.  | cost_center_upserted |
|  Deleção de Centro de Custo  | Empresa | Deleção de um centro de custo na empresa.  | É disparado quando é deletado um centro de custo na Xerpa.  | cost_center_deleted |
|  Alteração e Inclusão de Cargo | Empresa | Alteração ou inclusão de um cargo específico na empresa.  | É disparado quando é incluído ou alterado um cargo na Xerpa.  | position_upserted |
|  Deleção de Cargo | Empresa | Deleção de um novo cargo na empresa.  | É disparado quando é deletado um cargo na Xerpa.  | position_deleted |

##Criando um Webhook

Para criar um webhook na Xerpa, você deve acessar primeiro a página de configuração da empresa.

![Configuração de Empresa](/img/criar_webhook/configuracao_da_empresa.png)

Dentro da página de configuração da empresa, você encontrará a página Desenvolvedores & Xerpa API e lá poderá acessar o link para a página de administração de webhooks.

![Configuração de Empresa](/img/criar_webhook/desenvolvedores_xerpa_api.png)

![Configuração de Empresa](/img/criar_webhook/webhooks_link.png)

Ao acessar a página de administração dos webhooks, você encontrará todos os webhooks configurados e os eventos escutados por cada webhook. 

Para criar um Webhook, você pode clicar em "Novo Webhook".

![Configuração de Empresa](/img/criar_webhook/webhooks_adm.png)

A página a seguir aparecerá na sua lateral direita.

![Configuração de Empresa](/img/criar_webhook/webhooks_new.png)

Abaixo você encontra os valores necessários para a configuração do webhook. 

|  Campo | Descrição | Forma de Preenchimento |
| --- | --- | --- |
|  URL |  URL que receberá o Webhook quando este for disparado. | Alfanumérico |
|  Token | Token de autenticação na URL a receber o disparo. | Alfanumérico |
|  Eventos | São os eventos cadastrados no webhook para disparo quando um evento específico ocorrer. | Conferir **Tabela de Eventos e Entidades** acima  |

!!! note "Token da URL"
    O Token de autenticação cadastrado no webhook é referente a URL cadastrada, sendo facultativo o seu preenchimento caso a URL não necessite autenticação.






