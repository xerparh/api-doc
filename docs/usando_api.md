# Usando a API

Você precisará criar uma empresa no ambiente _sandbox_ antes de começar a usar a API. Para registrar um nova empresa, acesse [company-signup](https://sandbox.xerpa.com.br/company-signup) e complete o processo de registro.

Após a criação da conta, você deve gerar um token para continuar.

## Autenticação com token

O primeiro passo para utilizar a _API_ da Xerpa é solicitar um _token_ de autenticação. Esta requisição só poderá ser realizada por usuários do *Departamento Pessoal*.

Os passos para gerar o _token_ são descritos a seguir:

Acessar o [app][sandbox_app] da Xerpa com usuário de permissão de `Departamento pessoal`:

![imagem_login](/img/gerar_token/login.png)

Configuração da Empresa

![imagem_config_empresa](/img/gerar_token/config_empresa.png)

Xerpa API

![imagem_config_api](/img/gerar_token/config_xerpa_api.png)

Digite uma pequena descrição sobre o _token_

![imagem_config_type_token](/img/gerar_token/config_xerpa_api_type.png)

Clique no botão *Gerar token*

![imagem_config_gen_token](/img/gerar_token/get_token.png)

!!! danger "Atenção"
    Mantenha este _token_ seguro.  
    Considere-o como uma senha pois este _token_ permite ao usuário ler, escrever e apagar _qualquer_ informação associada a empresa e aos colaboradores.

Com o _token_ em mãos, você poderá adicioná-lo no cabeçalho de cada requisição HTTP, de acordo com o exemplo:

```
Authorization: Bearer TOKEN
```

!!! note "Observação"
    Lembre-se de substituir _TOKEN_ pelo valor obtido no passo anterior.

## Company ID

O Company ID é o identificador da empresa dentro da Xerpa. Com ele, você poderá realizar operações básicas dentro daquela empresa através da API e é 
essencial para as requisições, sendo recomendado que você envolva esse dado no processo de configuração da integração. 

Para conseguir o Company ID, basta solicitar à empresa usuária da Xerpa. 

## Escolhendo uma biblioteca

Para tornar a integração mais rápida, pra quem nunca trabalhou com GraphQl, nós selecionamos algumas bibliotecas para acelerar a implementação.

!!! note "Observação"
    O uso e a escolha das bibliotecas é de inteira responsabilidade de quem está consumindo a API.  
    Existem **várias** bibliotecas para cada uma das linguagens.  
    Caso queira ver mais opções, o [site oficial do Graphql][codeGraphQl] é uma ótima referência.

### C# .NET

[GraphQL.Client][dotnetclient]: Essa biblioteca é bem simples de usar.

### Java com Spring Boot
[GraphQL.Client][javaspringboot]: Tutorial com um dos principais frameworks para Java

### Python

[sgqlc][pythonclient]: Das bibliotecas informadas na documentação oficial, essa é a melhor mantida e com exemplos de uso com menos passos.

#### Exemplo

Criar o projeto em python:

```sh
mkdir xerpa-client
cd xerpa-client
echo "sgqlc" > requirements.txt
pip3 install -r requirements.txt
touch main.py
```

Conteúdo do main.py:

```python
from sgqlc.endpoint.http import HTTPEndpoint

url = 'https://sandbox.xerpa.com.br/api/g'
headers = {'Authorization': 'Bearer SEU_TOKEN_VEM_AQUI'}
graphqlInstance = HTTPEndpoint(url, headers)

# Altere o número com o ID da sua compania
variaveis = {"companyId": 1}

# O código GraphQl vem como string nessa variável
# não importa se é query ou se é mutation
codigoQueryGraphQl = """
query ($companyId: ID!) {
  company(id: $companyId) {
    profileSearch(query: "nome do colab") {
      profiles {
        name
        username
      }
    }
  }
}
"""

# O resultado é colocado na variável result
result = graphqlInstance(codigoQueryGraphQl, variaveis)
```

## Integrando sem bibliotecas

Um outra opção é fazer as requisições HTTP POST diretamente sem o auxílio de
bibliotecas.  
Isso é útil quando sua linguagem não possui bibliotecas para integrar, ou se
você realmente não quer utilizar uma lib.

As requisições são feitas todas com POST, não importando se você utilizará uma
`query` ou uma `mutation` do GraphQl.

### Corpo do POST

```
{
  "query": "query ($companyId: ID!){company(id: $companyId){profileSearch(query: \"nome do colab\"){profiles {name username}}}}",
  "variables": {"companyId": "90"}
}
```

!!! note "Observação"
    para que tudo corra bem, seu JSON precisa estar devidamente
    formatado. Veja que no exemplo, as aspas duplas dentro da string tem um
    caracter de escape.

[dotnetclient]: https://github.com/graphql-dotnet/graphql-client
[javaspringboot]: https://www.graphql-java.com/tutorials/getting-started-with-spring-boot/
[pythonclient]: https://github.com/profusion/sgqlc#usage
[codeGraphQl]: https://graphql.org/code/#graphql-clients
[sandbox_app]: https://sandbox.xerpa.com.br
