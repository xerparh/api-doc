# Atualização de Dados do Colaborador na Xerpa

Após o início do processo de admissão, todos os dados do colaborador ficam disponíveis para atualização na Xerpa, utilizando como chave o Identificador de perfil do colaborador.

Você pode monitorar o status da admissão através dos nossos [webhooks](/webhooks).

---

## Consultando um Colaborador por GUID
Você pode consultar um colaborador por GUID através da seguinte query:

```graphql

query ($guid: String!) {
  profileByGuid (guid: $guid) {
    name
    guid
    id
    # ... Dentro do playground voce encontrará todos os campos disponíveis
  }
}

```

---

# Manipulando informações de um perfil
Abaixo segue alguns exemplos de atualização de perfil. Você pode alterar de acordo com a sua necessidade.

## Dados Pessoais

#### Identificação
``` graphql
mutation ($guid: ID!) {
  profileIdentificationUpdate (input: {
    name: "Usuário 1"
    guid: $guid
    # ... Dentro do playground voce encontrará todos os campos disponíveis
  }) {
    profile {
      guid
      name
    # ... Dentro do playground voce encontrará todos os campos disponíveis
    }
  }
}

```

#### Contatos Pessoais
``` graphql
mutation ($profileId: ID!) {
  personalContact ( input: {
    profileId: $profileId
    email: "foo@bar.com"
    # ... Dentro do playground voce encontrará todos os campos disponíveis
  }) {
    profile {
      guid
      personalContact {
        email
        # ... Dentro do playground voce encontrará todos os campos disponíveis
      }
    }
  }
}

```

#### Contatos de Emergência
``` graphql
mutation ($profileId: ID!) {
  personalContact ( input: {
    profileId: $profileId
    emergency: {
      name: "Pessoa 1"
      relationship: "Pai"
      # ... Dentro do playground voce encontrará todos os campos disponíveis
    }
  }) {
    profile {
      guid
      emergencyContact {
        name
        # ... Dentro do playground voce encontrará todos os campos disponíveis
      }
    }
  }
}

```

#### Dados Bancários
``` graphql
mutation ($profileId: ID!) {
  bankAccountUpsert(input: {
    profileId: $profileId, # GUID type
    account: "123456",
    bankId: 1,
    agency: "000001",
    accountType: CHECKING
    # ... Dentro do playground voce encontrará todos os campos disponíveis
  }) {
    profile {
      guid
      bankAccounts {
        account
        # ... Dentro do playground voce encontrará todos os campos disponíveis
      }
    }
  }
}

```

#### Endereço Residencial
``` graphql
mutation ($profileId: ID!) {
  personalAddressUpsert(input: {
    profileId: $profileId,
    number: "20",
    attachments: {
      proofOfAddress: {
        name: null,
        action: NONE,
        url: null
      }
      # ... Dentro do playground voce encontrará todos os campos disponíveis
    }
  }) {
    profile {
      guid
      personalAddress {
        address1
        # ... Dentro do playground voce encontrará todos os campos disponíveis
      }
    }
  }
}

```

#### Formação Escolar
``` graphql
mutation ($profileId: ID!) {
  educationUpsert(input: {
    profileId: $profileId, # GUID type
    degree: BACHELOR
    situation: CONCLUDED
    school: "Marechal Deodoro"
    # ... Dentro do playground voce encontrará todos os campos disponíveis
  }) {
    profile {
      guid
      education {
        degree
        # ... Dentro do playground voce encontrará todos os campos disponíveis
      }
    }
  }
}

```

---

## Dados Contratuais

!!! warning "Informações da compania"
    Podem ser consultadas realizando uma busca nas entidades de **[company](/casos_de_uso_empresa/consultando_dados_organizacionais/)**

#### Contrato de Trabalho
```graphql
mutation ($profileId: ID!) {
  profileContractUpdate(input: {
    employmentContract: CLT,
    id: $profileId
    # ... Dentro do playground voce encontrará todos os campos disponíveis
  }) {
    guid
    # ... Dentro do playground voce encontrará todos os campos disponíveis
  }
}
```

#### Contato Profissional
``` graphql
mutation ($profileId: ID!) {
  businessContact(input: {
    profileId: $profileId,
    branch: "Amazonas"
    # ... Dentro do playground voce encontrará todos os campos disponíveis
  }) {
    profile {
      guid
      businessContact {
        branch
        # ... Dentro do playground voce encontrará todos os campos disponíveis
      }
    }
  }
}

```

---

## Documentos

#### Cartão de Saúde (SUS)
``` graphql
mutation ($profileId: ID!) {
  documentSusUpsert(input: {
    profileId: $profileId, # GUID type
    number: "1232456"
    # ... Dentro do playground voce encontrará todos os campos disponíveis
  }) {
    profile {
      guid
      documents {
        sus {
          ... on DocumentSus {
            number
            # ... Dentro do playground voce encontrará todos os campos disponíveis
          }
        }
      }
    }
  }
}

```

#### RG
``` graphql
mutation ($profileId: ID!) {
  documentRgUpsert ( input: {
    number: "1234567"
    profileId: $profileId # GUID type
    # ... Dentro do playground voce encontrará todos os campos disponíveis
  }) {
    profile {
      guid
      documents {
        rg {
          number
          # ... Dentro do playground voce encontrará todos os campos disponíveis
        }
      }
    }
  }
}

```

#### CPF
``` graphql
mutation ($profileId: ID!) {
  documentCpfUpsert(input: {
    profileId: $profileId, # GUID type
    number: "76500805445"
  }) {
    profile {
      guid
      documents {
        cpf {
          number
          # ... Dentro do playground voce encontrará todos os campos disponíveis
        }
      }
    }
  }
}

```

#### Carteira de Reservista
``` graphql
mutation ($profileId: ID!) {
  documentCamUpsert(input: {
    profileId: $profileId, # GUID type
    number: "123456"
  }) {
    profile {
      guid
      documents {
        cam {
          ... on DocumentCam {
            number
            # ... Dentro do playground voce encontrará todos os campos disponíveis
          }
        }
      }
    }
  }
}

```

#### CNH
``` graphql
mutation ($profileId: ID!) {
  documentCnhUpsert(input: {
    profileId: $profileId, # GUID type
    number: "123456"
    # ... Dentro do playground voce encontrará todos os campos disponíveis
  }) {
    profile {
      guid
      documents {
        cnh {
          ... on DocumentCnh {
            number
            # ... Dentro do playground voce encontrará todos os campos disponíveis
          }
        }
      }
    }
  }
}

```

---

# Dados do Colaborador

Você pode conferir todos os dados do colaborador disponíveis para **leitura e escrita [aqui](/dados_do_colaborador)**.

# Eventos

Você pode conferir todos os eventos referentes ao colaborador disponíveis para monitoramento [aqui](/webhooks).
