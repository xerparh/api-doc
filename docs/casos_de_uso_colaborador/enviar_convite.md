# Enviar Convite e Iniciar Admissão

Existem duas formas de iniciar a admissão de um colaborador na Xerpa:

- Enviando um email ou SMS para o colaborador iniciar o seu cadastro;
- Iniciar o processo de admissão unilateralmente via Xerpa;

Ambas as formas utilizam a mesma estrutura, como podemos ver abaixo:

```json
{
  "input": {
    "companyId": 1,
    "profiles": [
      {
        "employmentContract": "CLT",
        "name": "colaborador",
        "username": "colaborador@email.com",
        "admissionDate": "2027-10-10",
        "documentLimitDate": "2027-10-05"
      }
    ]
  }
}
```

O Regime de Contratação deve ser consultado através da seguinte query:

```graphql
query{
  enumDescriptions      {
    employmentContract  {
      label
      name
      value
    }
  }
}
```

Abaixo você pode admitir o colaborador e enviar notificação por E-mail ou SMS:

```graphql
mutation ($input: InviteInput!) {
  inviteProfiles(input: $input) {
    id
  }
}
```

E aqui admitir o colaborador **sem** enviar notificações;

```graphql
mutation ($input: InviteInput!) {
  manualInviteProfiles(input: $input) {
    id
  }
}
```

Para ambos os casos, o retorno será o ID do colaborador:

```json
{
  "data": {
    "inviteProfiles": [
      {
        "id": "1"
      }
    ]
  }
}
```

