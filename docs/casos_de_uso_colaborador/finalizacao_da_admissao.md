# Finalização da Admissão

Após a finalização da admissão, o colaborador fica disponível por completo para a consulta de dados. **Não recomendamos** a consulta durante o processo de admissão, pois os dados do colaborador ainda não foram conferidos pelo DP e também não foram preenchidos por completo.

Você pode utilizar nossos [webhooks](/webhooks) para monitorar a jornada de admissão do colaborador.

# Consultando um Colaborador

Você pode consultar um colaborador através da seguinte query:

```graphql
{
  profile(id: 24523) {
    id
    guid
    name
    guid
    username
    access_situation
    admission_date
    probation_date1
    probation_date2
    trusted_position
    salary
    birthday
    employment_contract
    marital_status
    status
    gender
    nationality
    naturalityCity {
      name
      state {
        name
        code
        country {
          name
        }
      }
    }
   }
  }

```

# Consulta em Massa de Colaboradores

Você pode consultar todos os colaboradores de uma empresa através da seguinte query:

```graphql

query ($company_id: ID!, $query: String!) {
 company (id: $company_id) {
   profileSearch(query: $query ) {
     profiles {
       id
       }
     }
   }
 }

```


# Dados do Colaborador

Você pode conferir todos os dados do colaborador disponíveis para **leitura e escrita [aqui](/dados_do_colaborador)**.

# Eventos

Você pode conferir todos os eventos referentes ao colaborador disponíveis para monitoramento [aqui](/webhooks).
